package DuAn2.user;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
@RequestMapping({"/"})
public class PageController {
	

	
	@RequestMapping(value = "about", method = RequestMethod.GET)
	public String flowers() {
		return "about";
	}
	
	@RequestMapping(value = "service", method = RequestMethod.GET)
	public String service() {
		return "service";
	}
	
	@RequestMapping(value = "blog", method = RequestMethod.GET)
	public String blog() {
		return "blog";
	}
	
	@RequestMapping(value = "blog-single", method = RequestMethod.GET)
	public String blog_single() {
		return "blog-single";
	}
	
	@RequestMapping(value = "room", method = RequestMethod.GET)
	public String room() {
		return "room";
	}
	
	@RequestMapping(value = "room-single", method = RequestMethod.GET)
	public String room_single() {
		return "room-single";
	}
	
	@RequestMapping(value = "contact", method = RequestMethod.GET)
	public String contact() {
		return "contact";
	}
	
	@RequestMapping(value = "booking", method = RequestMethod.GET)
	public String booking() {
		return "booking";
	}
	
	@RequestMapping(value = "listroom", method = RequestMethod.GET)
	public String listroom() {
		return "listroom";
	}
}
